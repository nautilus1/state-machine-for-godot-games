extends Area2D

export var speed: float = 200;
var direction: Vector2;


func _ready() -> void:
	$Anim.play("Fire");


func _physics_process(delta) -> void:
	move_local_x(direction.x * speed * delta);
	
	if direction.y == 1:
		move_local_y(direction.y * (speed*2) * delta);
	else:
		move_local_y(direction.y * speed * delta);


func _on_Bullet_body_entered(body):
	queue_free();


func _on_Timer_timeout():
	queue_free();
