extends "res://actors/platformer_char.gd"

onready var gun_pos = $pivot/gun_pos;
onready var reload_time = $Reload;

var default_bullet = load("res://actors/bullet/Bullet.tscn");

var bullet_direction := Vector2();


func _ready() -> void:
	state_machine = $StateMachine;
	animation_player = $Anim;
	
	state_machine.start();


func _process(delta) -> void:
	if Input.is_action_pressed("attack"):
		fire(default_bullet, bullet_direction.x, bullet_direction.y);


func _physics_process(delta) -> void:
	.apply_gravity(delta);


func fire(default_bullet_l, direction_x = direction.x, direction_y = 0) -> void:
	if !self.reload_time.is_stopped():
		return
	
	if !direction_x and !direction_y: # if direction.x == 0 and direction.y == 0;
		direction_x = $pivot.scale.x;
	
	var new_bullet = default_bullet_l.instance();
	self.get_parent().add_child(new_bullet);
	new_bullet.global_position = self.gun_pos.global_position;
	new_bullet.direction = Vector2(direction_x, direction_y);
	
	self.reload_time.start();