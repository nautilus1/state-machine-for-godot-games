extends "res://scripts/platformer_default_states/jump_default_state.gd"

func enter():
	self_anim_name = "Jump";
	.enter();


func handle_input(event):
	if Input.is_action_just_released("attack"):
		anim_character.play("Jump");
	
	.handle_input(event);


func update(delta):
	if Input.is_action_pressed("attack") and Input.is_action_pressed("ui_up") and anim_character.current_animation != "JumpTopFire":
		anim_character.play("JumpTopFire");
		character.bullet_direction = Vector2(0, -1);
	
	if  Input.is_action_pressed("attack") and Input.is_action_pressed("ui_down") and anim_character.current_animation != "JumpDownFire":
		anim_character.play("JumpDownFire");
		character.bullet_direction = Vector2(0, 1);
	
	if Input.is_action_pressed("attack") and (Input.is_action_pressed("ui_right") or Input.is_action_pressed("ui_left")) and anim_character.current_animation != "JumpFire":
		anim_character.play("JumpFire");
		character.bullet_direction = Vector2(0, 0);
	
	.update(delta);


func exit() -> void:
	character.bullet_direction = Vector2(0, 0);