extends "res://scripts/platformer_default_states/state.gd"


func enter() -> void:
	character.velocity.x = 0;
	anim_character.play("Down");


func handle_input(event):
	if Input.is_action_just_released("ui_down"):
		parent.change_state("Idle");
	
	if Input.is_action_just_pressed("ui_up"):
		parent.change_state("Run");


func update(delta):
	if Input.is_action_pressed("attack") and anim_character.current_animation != "DownFire":
		anim_character.play("DownFire");
	
	.update(delta);