extends "res://scripts/platformer_default_states/idle_default_state.gd"


func enter() -> void:
	self_anim_name = "Idle";
	
	if not Input.is_action_pressed("attack"):
		.enter();


func handle_input(event) -> void:
	if Input.is_action_just_pressed("ui_up") and !Input.is_action_pressed("attack"):
		parent.change_state("Jump");
	
	if Input.is_action_just_pressed("ui_down"):
		parent.change_state("Down");


func update(delta):
	if Input.is_action_pressed("attack") and Input.is_action_pressed("ui_up") and anim_character.current_animation != "TopFire":
		anim_character.play("TopFire");
		character.bullet_direction = Vector2(0, -1);
	elif Input.is_action_pressed("attack") and not Input.is_action_pressed("ui_up") and anim_character.current_animation != "IdleFire":
		anim_character.play("IdleFire");
		character.bullet_direction = Vector2(direction.x, 0);
	elif not Input.is_action_pressed("attack") and anim_character.current_animation != "Idle":
		anim_character.play(self_anim_name);
	
	.update(delta);


func exit() -> void:
	character.bullet_direction = Vector2(0, 0);