extends "res://scripts/platformer_default_states/run_default_state.gd"


func enter() -> void:
	self_anim_name = "Run";
	if not Input.is_action_pressed("attack"):
		.enter();


func handle_input(event) -> void:
	if Input.is_action_just_released("attack"):
		anim_character.play("Run");
	
	if Input.is_action_just_pressed("ui_up") and not Input.is_action_pressed("attack"):
		parent.change_state("Jump");
	
	if Input.is_action_just_pressed("ui_down"):
		parent.change_state("Down");
		print("asas")


func update(delta) -> void:
	get_direction();
	
	if Input.is_action_pressed("attack") and Input.is_action_pressed("ui_up") and anim_character.current_animation != "RunTopFire":
		anim_character.play("RunTopFire");
		character.bullet_direction = Vector2(0, -1);
	elif Input.is_action_pressed("attack") and not Input.is_action_pressed("ui_up") and anim_character.current_animation != "RunFire":
		anim_character.play("RunFire");
		character.bullet_direction = Vector2(direction.x, 0);
	
	if not direction.x:
		parent.change_state("Idle");
	
	move(delta);


func exit() -> void:
	character.bullet_direction = Vector2(0, 0);