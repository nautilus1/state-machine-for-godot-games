extends "res://actors/top_down_char.gd"

onready var camera = $camera;

func _ready() -> void:
	state_machine = $StateMachine;
	animation_player = $Anim;
	
	state_machine.start();