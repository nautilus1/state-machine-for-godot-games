extends "res://scripts/top_down_default_states/state.gd"

func enter() -> void:
	get_direction();
	anim_character.stop();


func update(delta):
	set_anim_direction();


func set_anim_direction():
	if anim_character.is_playing() == false:
		if !direction.x and direction.y == -1:
			anim_character.play("AttackBowBack");
		elif !direction.x and direction.y == 1:
			anim_character.play("AttackBowFront");
		elif direction.x and !direction.y:
			anim_character.play("AttackBowSide");
		
		yield(anim_character, "animation_finished");
		
		parent.change_state("Idle");