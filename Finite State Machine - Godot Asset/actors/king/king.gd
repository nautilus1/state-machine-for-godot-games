extends "res://actors/platformer_char.gd"

onready var detect_enemy = $pivot/detect_enemy;

var enemies: Array;


func _ready() -> void:
	animation_player = $Anim;
	state_machine = $StateMachine;
	
	state_machine.start();


func _on_detect_enemy_body_entered(body: KinematicBody2D):
	if body and body.is_in_group("Enemies"):
		enemies.append(body);


func _on_detect_enemy_body_exited(body: KinematicBody2D):
	if body and body in enemies:
		enemies.erase(body);
