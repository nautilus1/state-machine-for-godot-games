extends "res://scripts/platformer_default_states/state.gd"


func enter() -> void:
	attack();
	
	anim_character.play("Attack");
	anim_character.connect("animation_finished", self, "attacked");


func attack():
	if character.enemies.size() != 0:
		for enemy in character.enemies:
			if not enemy.state_machine.current_state:
				continue
			
			if not enemy.state_machine.current_state.name in ["Hit", "Dead"]:
				enemy.state_machine.change_state("Hit");
				break


func attacked(anim_name) -> void:
	if anim_name == "Attack":
		parent.change_state("Idle");
		anim_character.disconnect("animation_finished", self, "attacked");