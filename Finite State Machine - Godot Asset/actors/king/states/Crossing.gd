extends "res://scripts/state.gd"


func enter() -> void:
	if character.crossing:
		anim_character.play("DoorOut");
	else:
		anim_character.play("DoorIn");
	
	anim_character.connect("animation_finished", self, "crossing");


func crossing(anim_name) -> void:
	if anim_name in ["DoorOut", "DoorIn"]:
		parent.change_state("Idle");
		anim_character.disconnect("animation_finished", self, "crossing");