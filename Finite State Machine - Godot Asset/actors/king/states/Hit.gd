extends "res://scripts/platformer_default_states/state.gd"


func enter() -> void:
	self_anim_name = "Hit";
	anim_character.play(self_anim_name);
	anim_character.connect("animation_finished", self, "hited");


func handle_input(event) -> void:
	if Input.is_action_just_pressed("attack"):
		parent.change_state("Attack");
	

func hited(anim_name) -> void:
	if anim_name == self_anim_name:
		parent.change_state("Jump");
		anim_character.disconnect("animated_finished", self, "hited");