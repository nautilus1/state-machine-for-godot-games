extends "res://scripts/platformer_default_states/jump_default_state.gd"


func enter() -> void:
	self_anim_name = "Jump";
	.enter();


func handle_input(event) -> void:
	if Input.is_action_just_pressed("attack"):
		parent.change_state("Attack");
		
	.handle_input(event);
	