extends "res://scripts/platformer_default_states/run_default_state.gd"


func enter() -> void:
	self_anim_name = "Run";
	anim_character.play("Run");


func handle_input(event) -> void:
	if Input.is_action_just_pressed("attack"):
		parent.change_state("Attack");
	
	if Input.is_action_just_pressed("jump"):
		parent.change_state("Jump");