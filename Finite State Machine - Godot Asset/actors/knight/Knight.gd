extends "res://actors/platformer_char.gd"

onready var sword_detect = $pivot/sword_detect;
onready var combo_time = $combo_time;

func _ready() -> void:
	sword_detect.add_exception(self);
	
	state_machine = $StateMachine;
	animation_player = $Anim;
	state_machine.start();