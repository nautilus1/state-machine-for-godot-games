extends "res://scripts/platformer_default_states/state.gd"

func enter() -> void:
	anim_character.play("GuardStart");
	yield(anim_character, "animation_finished");
	anim_character.play("Guard");


func handle_input(event) -> void:
	if Input.is_action_just_pressed("attack"):
		parent.change_state("Attack");
	
	if Input.is_action_just_pressed("ui_up"):
		parent.change_state("Jump");
	
	if Input.is_action_just_pressed("shield"):
		parent.change_state("Idle");


func update(delta) -> void:
	get_direction();
	
	if direction.x and anim_character.current_animation != "GuardStart":
		parent.change_state("Run");