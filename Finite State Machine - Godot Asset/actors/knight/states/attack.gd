extends "res://scripts/platformer_default_states/state.gd"

var num_attacks: Array;
var current_attack: int;
var second_attack: bool;
var strength: float;

var sword_detect: RayCast2D;


func enter() -> void:
	anim_character.connect("animation_finished", self, "attack_finished");
	attack();


func handle_input(event) -> void:
	if Input.is_action_just_pressed("shield"):
		parent.change_state("Shield");


func attack() -> void:
	sword_detect = character.sword_detect;
	
	match current_attack:
		0:
			anim_character.play("Attack1");
		1:
			anim_character.play("Attack2");
		2:
			anim_character.play("Attack3");
	
	if sword_detect.is_colliding():
		var body = sword_detect.get_collider();
		
		if body.is_in_group("Enemy"):
			body.state_machine.change_state("Hit");
	
	current_attack += 1;


func attack_finished(anim_name) -> void:
	anim_character.disconnect("animation_finished", self, "attack_finished")
	parent.change_state("Idle");
	character.combo_time.start();
	
	if current_attack == character.max_attacks:
		current_attack = 0;


func _on_combo_time_timeout() -> void:
	if parent.current_state != self:
		current_attack = 0;