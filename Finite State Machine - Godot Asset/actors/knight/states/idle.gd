extends "res://scripts/platformer_default_states/idle_default_state.gd"


func enter() -> void:
	self_anim_name = "Idle";
	.enter();


func handle_input(event) -> void:
	if Input.is_action_just_pressed("ui_up"):
		parent.change_state("Jump");
	
	if Input.is_action_just_pressed("attack"):
		parent.change_state("Attack");
	
	if Input.is_action_pressed("shield"):
		parent.change_state("Shield");


func update(delta):
	.update(delta);