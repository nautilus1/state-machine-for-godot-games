extends "res://scripts/platformer_default_states/state.gd"

var default_bullet = load("res://actors/bullet/Bullet.tscn");


func enter() -> void:
	anim_character.play("Cling");


func handle_input(event):
	if Input.is_action_just_pressed("ui_up"):
		character.set_physics_process(true);
		parent.change_state("Jump");


func update(delta):
	on_wall();


func on_wall():
	direction.x = parent.last_direction.x;
	
	character.set_physics_process(false);
	character.get_node("pivot").scale.x = direction.x
	character.direction.x = direction.x;
