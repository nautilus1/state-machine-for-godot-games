extends "res://scripts/platformer_default_states/idle_default_state.gd"

func enter():
	self_anim_name = "Idle";
	.enter();


func handle_input(event) -> void:
	if Input.is_action_just_pressed("ui_up"):
		parent.change_state("Jump");
