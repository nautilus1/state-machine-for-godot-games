extends "res://scripts/platformer_default_states/jump_default_state.gd"

var default_bullet = load("res://actors/bullet/Bullet.tscn");


func enter():
	self_anim_name = "Jump";
	.enter();


func update(delta):
	.update(delta);
	
	if character.is_on_wall() and character.wall_detect.is_colliding() and !character.wall_detect.get_collider().is_in_group("Characters"):
		var wall = character.get_slide_collision(character.get_slide_count()-1);
		parent.last_direction = wall.normal;
		parent.change_state("Cling");
