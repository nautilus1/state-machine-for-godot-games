extends "res://scripts/platformer_default_states/run_default_state.gd"

func enter() -> void:
	self_anim_name = "Run";
	.enter();


func handle_input(event) -> void:
	if Input.is_action_just_pressed("ui_up"):
		parent.change_state("Jump");


func update(delta):
	if Input.is_action_pressed("attack"):
		parent.change_state("Run_shoot");
	
	.update(delta)
	
