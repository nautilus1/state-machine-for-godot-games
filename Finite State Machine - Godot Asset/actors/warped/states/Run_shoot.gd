extends "res://scripts/platformer_default_states/run_default_state.gd"

var default_bullet = load("res://actors/bullet/Bullet.tscn");


func enter() -> void:
	character.default_bullet = default_bullet;
	self_anim_name = "Run_shoot";
	.enter();


func handle_input(event) -> void:
	if Input.is_action_just_pressed("ui_up"):
		parent.change_state("Jump");
