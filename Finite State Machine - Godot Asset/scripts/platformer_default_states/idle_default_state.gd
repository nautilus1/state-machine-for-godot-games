extends "res://scripts/platformer_default_states/state.gd"


func enter():
	anim_character.play(self_anim_name);


func update(delta) -> void:
	get_direction()
	
	if direction:
		parent.change_state("Run");