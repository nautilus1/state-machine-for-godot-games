extends "res://scripts/platformer_default_states/state.gd"


func enter() -> void:
	anim_character.play(self_anim_name);


func update(delta) -> void:
	get_direction();
	
	if !direction.x:
		parent.change_state("Idle");
	
	move(delta);
