extends Node

onready var parent: Node = get_parent(); # StateMachine is the parent;
var character: KinematicBody2D = owner; # Player or NPc node is the Owner;

var anim_character: AnimationPlayer = null;
var self_anim_name: String = "";

var speed: float;

var velocity := Vector2();
var direction := Vector2();


func enter() -> void:
	pass


func update(delta) -> void:
	pass


func handle_input(event) -> void:
	pass


func get_direction() -> void:
	if character.is_in_group("Players"):
		direction.x = int(Input.is_action_pressed("ui_right"))-int(Input.is_action_pressed("ui_left"));
		character.direction = direction;
	
	if direction.x != 0:
		character.get_node("pivot").scale = Vector2(direction.x, 1);


func move(delta) -> void:
	character.velocity.x = character.speed * direction.x;


func exit() -> void:
	character.velocity.x = 0;
