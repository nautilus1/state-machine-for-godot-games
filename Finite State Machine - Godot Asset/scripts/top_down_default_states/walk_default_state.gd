extends "res://scripts/top_down_default_states/state.gd"


func enter() -> void:
	pass


func update(delta) -> void:
	get_direction();
	
	if !direction.x and !direction.y:
		parent.change_state("Idle");
	
	move(delta);