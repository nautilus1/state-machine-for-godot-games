🇬🇧 ️A state machine with some demonstrations that can adjust directly to your projects.
🇪🇸 ️Una máquina de estados con algunas demostraciones que pueden ajustar directamente a sus proyectos. 

[![License: GPL v3](https://img.shields.io/badge/License-GPL%20v3-blue.svg)](https://github.com/alainm23/planner/blob/master/LICENSE)
[![Donate](https://img.shields.io/badge/PayPal-Donate-gray.svg?style=flat&logo=paypal&colorA=0071bb&logoColor=fff)]("https://paypal.me/joseleon1971")

<div align="center">
  <span align="center"> <img width="80" height="90" src="https://pbs.twimg.com/profile_images/1211845531020021760/x3NlGNpu_400x400.png" alt="Icon"></span>
  <h1 align="center">State Machine for Godot Games</h1>
  <h3 align="center">Add characters with ease. </h3>

 <iframe frameborder="0" src="https://itch.io/embed/512672?border_width=0&amp;bg_color=262626&amp;fg_color=fff&amp;link_color=FBCA97&amp;border_color=515151" width="206" height="165"><a href="https://in3mo.itch.io/state-machine-for-godot-games">Finite State Machine - Godot Asset by César León</a></iframe>


<p align="center">
    <img src="https://img.itch.zone/aW1nLzI4NTQ4NzAucG5n/original/LDJuuT.png">
</p>

</div>

## Instalation

You will need:

* [Godot Engine 3.2 or Godot Engine 3.1](https://godotengine.org/download/linux)

## How to use in a project 

* First create the following folders within your project: ![p1](https://i.ibb.co/S0yFM97/1-1.png)

* Import the following scripts: ![p2](https://i.ibb.co/VpkYjjq/2-1.png)

* Import "platformer_char.tscn/gd" and "top_down_char.tscn/gd": ![p3](https://i.ibb.co/KztWVBj/3-1.png)

* To create a new character, click on "New Inherited scene": ![p4](https://i.ibb.co/pW27T7w/5-1.png)

* Select character type: ![p5](https://i.ibb.co/ZHY53vq/6-1.png)

* Change the name of the main node and add the states and sprite image you want:
![p5](https://i.ibb.co/zsk9WMB/7.png)

* Each script for a new state must inherit from the script "state" specific to the type of character. There is a state for top-down characters and another for platforms: 
![p6](https://i.ibb.co/4NCbhX5/8.png)

## Credits

* Pixel Art:
    * [unTied Games](https://untiedgames.itch.io/) Valiant Knight.
    * [Ansimuz](https://ansimuz.itch.io/) Warped.
    * [Pixel Frog](https://pixel-frog.itch.io/) Kings and Pigs.
    * [Vaca Roxa](https://bakudas.itch.io/generic-run-n-gun) Generic "Contra".
* GDScript:
    * [GDQuest](https://www.gdquest.com/) Finite State Machine.
    * [PigDev](https://www.youtube.com/channel/UCFK9ZoVDqDgY6KGMcHEloFw) and [Game Endeavor](https://www.youtube.com/channel/UCLweX1UtQjRjj7rs_0XQ2Eg) Video series.

Thank you very much to all these great people for their contributions to this project; This finite state machine is an adaptation of the example published by GDQuest for Godot 3.0.

## Support

You can help me continue with the development of assets and demonstrations if you buy this resource at itch.io or send a donation by PayPal or Patreon.
* [PayPal](https://paypal.me/joseleon1971).
* [Patreon](https://www.patreon.com/indielibre).
* [Itch.io](https://in3mo.itch.io/state-machine-for-godot-games).

Visit [indielibre.com](https://indielibre.com/) for tutorials on these resources or extra information.